import React, { Component } from "react";
import { Text, View, Image, Dimensions, FlatList, Button } from "react-native";

const { width, height } = Dimensions.get("window");

export default class Feed extends Component {
  _renderItem = ({ item }) => {
    return (
      <View style={{ padding: 10 }}>
        <Text style={{ paddingVertical: 5 }}>{item.time}</Text>
        <View style={{ backgroundColor: "#fff", padding: 10, elevation: 5 }}>
          <Text>{item.text.primary}</Text>
          <Text>{item.text.secondary}</Text>
        </View>
        {item.appInfo && (
          <View
            style={{
              flexDirection: "row",
              backgroundColor: "#f2f2f2",
              alignItems: "center",
              justifyContent: "space-between",
              elevation: 5,
              padding: 10
            }}>
            <Image
              source={require("./assets/ola.jpeg")}
              style={{ width: 50, height: 50 }}
            />
            {item.appInfo.map((info, index) => (
              <View key={index}>
                <Text>{info.title}</Text>
                <Text>{info.text}</Text>
              </View>
            ))}
            <View>
              <Text>{item.appActionInfo}</Text>
              <Button title={item.appAction} onPress={() => null} />
            </View>
          </View>
        )}
      </View>
    );
  };

  _renderItemSeperatorComponent = () => {
    return (
      <View>
        <View
          style={{
            width: 10,
            height: 10,
            borderRadius: 5,
            backgroundColor: "#000",
            marginHorizontal: 25,
            marginVertical: 5
          }}
        />
        <View
          style={{
            width: 10,
            height: 10,
            borderRadius: 5,
            backgroundColor: "#000",
            marginHorizontal: 25,
            marginVertical: 5
          }}
        />
        <View
          style={{
            width: 10,
            height: 10,
            borderRadius: 5,
            backgroundColor: "#000",
            marginHorizontal: 25,
            marginVertical: 5
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={[this.props.style]}>
        <View
          style={{
            height: 75,
            backgroundColor: "#00aeef",
            flexDirection: "row",
            width,
            alignItems: "center",
            justifyContent: "space-around"
          }}>
          <View style={{ paddingRight: 15 }}>
            <Image
              style={{ width: 50, height: 50, borderRadius: 25 }}
              source={require("./assets/avatar.jpg")}
            />
          </View>
          <View style={{ flexDirection: "column" }}>
            <Text style={{ color: "#fff" }}>You are at</Text>
            <Text style={{ color: "#fff" }}>Home(Springfield, Bangalore)</Text>
            <Text style={{ color: "#fff" }}>Good Mornin! Aamer</Text>
          </View>
        </View>
        <Text style={{ padding: 10, fontSize: 20 }}>Feed</Text>
        <FlatList
          data={this.props.feed}
          extraData={this.props.feed}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this._renderItemSeperatorComponent}
        />
      </View>
    );
  }
}
