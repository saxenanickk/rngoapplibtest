import React, { Component, Fragment } from "react";
import { Text, View, StyleSheet } from "react-native";
import {
  widthPercentageToDP as WPD,
  heightPercentageToDP as HPD
} from "./Responsive";
import Feed from "./Feed";

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     paddingTop: StatusBar.currentHeight,
//     alignItems: "flex-start",
//     justifyContent: "flex-start",
//     backgroundColor: "#f4f3f8"
//   }
// });

export default class App extends Component {
  state = {
    feed: [
      {
        time: "09:00 AM",
        text: {
          primary: "Welcome to Mumbai",
          secondary: "1hr 17mins (25km) to Taj Mahal Tower"
        },
        appName: "ola",
        appInfo: [
          {
            title: "ETA",
            text: "3 mins"
          },
          {
            title: "OLA",
            text: "prime"
          }
        ],
        appAction: "Book",
        appActionInfo: "$ 215",
        appDeepLink: "some-deep-link"
      },
      {
        time: "10:00 AM",
        text: {
          primary: "You are at Kempagowda International airport",
          secondary: "Here are your flight details"
        },
        appName: "cleartrip",
        appInfo: [
          {
            title: "Bangalore to Mumbai",
            text: "Jet airways 9W 448"
          }
        ],
        appAction: "View Ticket",
        appActionInfo: "PDNEJOH",
        appDeepLink: "some-deep-link"
      },
      {
        time: "09:00 AM",
        text: {
          primary: "Welcome to Mumbai",
          secondary: "1hr 17mins (25km) to Taj Mahal Tower"
        },
        appName: "ola",
        appInfo: [
          {
            title: "ETA",
            text: "3 mins"
          },
          {
            title: "OLA",
            text: "prime"
          }
        ],
        appAction: "Book",
        appActionInfo: "$ 215",
        appDeepLink: "some-deep-link"
      },
      {
        time: "10:00 AM",
        text: {
          primary: "You are at Kempagowda International airport",
          secondary: "Here are your flight details"
        },
        appName: "cleartrip",
        appInfo: [
          {
            title: "Bangalore to Mumbai",
            text: "Jet airways 9W 448"
          }
        ],
        appAction: "View Ticket",
        appActionInfo: "PDNEJOH",
        appDeepLink: "some-deep-link"
      }
    ]
  };

  render() {
    return (
      <Fragment>
        {/* <FirstComponent /> */}
        {/* <Text style={{ fontSize: HPD("2%") }}>{`Hello World!`}</Text> */}
        <Feed
          feed={this.state.feed}
          style={{
            width: WPD("100%"),
            height: HPD("80%"),
            backgroundColor: "red"
          }}
        />
        <SecondComponent />
      </Fragment>
    );
  }
}

class FirstComponent extends Component {
  constructor() {
    super();
    this.state = {
      a: 0
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ a: this.state.a + 2 }), 1000);
  }

  render() {
    console.log("First Component");
    return (
      <View
        style={{
          width: WPD("100%"),
          height: HPD("50%"),
          backgroundColor: this.state.a ? "red" : "grey"
        }}
      />
    );
  }
}

class SecondComponent extends Component {
  constructor() {
    super();
    this.state = {
      a: 0
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ a: this.state.a + 2 }), 1000);
  }

  render() {
    console.log("Second Component");
    return (
      <View
        style={{
          width: WPD("100%"),
          height: HPD("20%"),
          backgroundColor: this.state.a ? "blue" : "black"
        }}
      />
    );
  }
}
